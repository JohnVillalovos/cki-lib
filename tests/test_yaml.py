"""Yaml util tests."""
import pathlib
import tempfile
import typing
import unittest

import responses
import yaml

from cki_lib import yaml as cki_yaml


class TestYaml(unittest.TestCase):
    """Test yaml utils."""

    @staticmethod
    def _dump(what: typing.Any) -> str:
        return yaml.dump(what, Dumper=cki_yaml.BlockDumper)

    def test_normal_string(self) -> None:
        """Check block string dumping without line feeds."""
        self.assertEqual(self._dump({"key": "value"}), "key: value\n")

    def test_line_feed(self) -> None:
        """Check block string dumping with line feeds."""
        self.assertEqual(self._dump({"key": "value\nline 2"}),
                         "key: |-\n  value\n  line 2\n")

    def test_valid_data(self) -> None:
        """Check roundtripping some data."""
        data = {"a": "b",
                "c": ["d", {"e": "f\ng", "h": "i", "k\nl": ["m"]}]}
        self.assertEqual(data, yaml.safe_load(self._dump(data)))

    def test_read_yaml_file(self) -> None:
        """Check reading YAML files."""
        with tempfile.NamedTemporaryFile() as yaml_file:
            yaml_file.write(b'foo: bar')
            yaml_file.flush()
            self.assertEqual(cki_yaml.load(file_path=yaml_file.name),
                             {'foo': 'bar'})

    def test_read_yaml_contents(self) -> None:
        """Check reading YAML from a string."""
        self.assertEqual(cki_yaml.load(contents='foo: bar'), {'foo': 'bar'})
        self.assertEqual(cki_yaml.load(contents='{}'), {})
        self.assertEqual(cki_yaml.load(), None)

    def test_resolve_reference_dict(self) -> None:
        """Check resolving references."""
        contents = 'foo: {bar: baz}\nqux: !reference [foo, bar]'
        self.assertEqual(cki_yaml.load(contents=contents, resolve_references=True),
                         {'foo': {'bar': 'baz'}, 'qux': 'baz'})

    def test_resolve_reference_list(self) -> None:
        """Check resolving references."""
        contents = 'foo: {bar: baz}\nqux: [!reference [foo, bar]]'
        self.assertEqual(cki_yaml.load(contents=contents, resolve_references=True),
                         {'foo': {'bar': 'baz'}, 'qux': ['baz']})

    def test_resolve_reference_no_dict(self) -> None:
        """Check resolving references doesn't work for non-dict roots."""
        contents = '[foo, bar: !reference [0]]'
        self.assertRaises(Exception,
                          lambda: cki_yaml.load(contents=contents, resolve_references=True))

    def test_include_missing(self) -> None:
        """Check .include is not required."""
        contents = 'foo: {bar: baz}'
        self.assertEqual(cki_yaml.load(contents=contents, resolve_includes=True),
                         {'foo': {'bar': 'baz'}})

    def test_include_non_dict(self) -> None:
        """Check non-dicts and .include support don't collide."""
        self.assertEqual(cki_yaml.load(contents='[foo, {bar: baz}]', resolve_includes=True),
                         ['foo', {'bar': 'baz'}])
        self.assertEqual(cki_yaml.load(contents='foo', resolve_includes=True),
                         'foo')

    @responses.activate
    def test_include_url(self) -> None:
        """Check resolving url includes."""
        contents = '.include: https://host/bar\nfoo: {bar: baz}'
        responses.add(responses.GET, 'https://host/bar',
                      json={'foo': {'bar': 'brrr', 'qux': 'fred'}})
        self.assertEqual(cki_yaml.load(contents=contents, resolve_includes=True),
                         {'foo': {'bar': 'baz', 'qux': 'fred'}})

    def test_include_file(self) -> None:
        """Check resolving url includes."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'main.yml').write_text('.include: [inc.yml]\nfoo: {bar: baz}')
            pathlib.Path(directory, 'inc.yml').write_text('foo: {bar: brrr, qux: fred}')
            self.assertEqual(cki_yaml.load(file_path=f'{directory}/main.yml',
                                           resolve_includes=True),
                             {'foo': {'bar': 'baz', 'qux': 'fred'}})

    def test_include_no_list(self) -> None:
        """Check resolving includes doesn't work for weird .include."""
        contents = '.include: {foo: bar}'
        self.assertRaises(Exception,
                          lambda: cki_yaml.load(contents=contents, resolve_includes=True))
