"""Test utilities to run functions with a timeout."""
import time
import unittest

from cki_lib.timeout import FunctionTimeout
from cki_lib.timeout import func_timeout
from cki_lib.timeout import timeout


class TestTimeout(unittest.TestCase):
    """Test timeout functions."""

    def test_func_timeout(self):
        """Ensure func_timeout works."""
        def func1():
            time.sleep(1)

        with self.assertRaises(FunctionTimeout):
            func_timeout(func1, 0.1)

    def test_timeout_decorator(self):
        """Ensure decorator works."""

        @timeout(0.1)
        def func2():
            time.sleep(1)

        with self.assertRaises(FunctionTimeout):
            func2()
