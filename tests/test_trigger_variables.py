"""Test trigger_variables."""
import os
import unittest
from unittest import mock

from cki_lib import trigger_variables


class TestTriggerVariables(unittest.TestCase):
    """Test trigger_variables."""

    @mock.patch.dict(os.environ, {'commit_hash': 'deadbeef', 'title': 'title',
                                  'ignored_var': 'dontcare'})
    def test_pipeline_vars_from_env(self) -> None:
        """Ensure pipeline_vars_from_env works."""
        self.assertEqual(trigger_variables.pipeline_vars_from_env(), {'commit_hash': 'deadbeef',
                                                                      'title': 'title'})

    @staticmethod
    def test_print_var_names() -> None:
        """Ensure print_var_names works."""
        with mock.patch('builtins.print') as mock_print:
            trigger_variables._print_var_names()  # pylint: disable=protected-access
        mock_print.assert_called()
