"""NonDaemonic pool tester."""
import unittest

from cki_lib.pool import NonDaemonicPool


def func(var_x):
    """Return a double of var_x."""
    return var_x * 2


class TestPool(unittest.TestCase):
    """Test NonDaemonicPool class."""

    def test_pool(self):
        # pylint: disable=protected-access
        """Ensure pool runs a non-daemonic proces."""
        with NonDaemonicPool(2) as pool:
            out_data = list(pool.imap(func, [1, 2]))

            self.assertEqual([2, 4], out_data)
            self.assertFalse(pool._ctx.Process().daemon)
