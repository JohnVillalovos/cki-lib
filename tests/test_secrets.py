"""Tests for the cki_lib.secrets module."""
import contextlib
import os
import pathlib
import subprocess
import tempfile
import typing
import unittest
from unittest import mock

import yaml

from cki_lib import secrets


@unittest.skipUnless(b'pbkdf' in subprocess.run(
    ['openssl', 'enc', '-help'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=False
).stdout, 'OpenSSL version too old')
class TestSecrets(unittest.TestCase):
    """Test yaml utils."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
            variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[str]:
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                pathlib.Path(directory, name).write_text(yaml.safe_dump(values), encoding='utf8')
            yield directory

    def test_variables(self) -> None:
        """Check that basic ordered variable processing works."""
        with self._setup_secrets({
                'public.yml': {'bar': 0, 'foo': 'puh'},
                'internal.yml': {'foo': 'qux', 'sec': 'none'},
                'secrets.yml': {'sec': 'ure'},
        }) as directory, mock.patch.dict(os.environ, {
            'PUBLIC_VARS_FILE': f'{directory}/public.yml',
            'INTERNAL_VARS_FILE': f'{directory}/internal.yml',
            'LEGACY_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertEqual(secrets.get_variable('bar'), 0)
            self.assertEqual(secrets.get_variable('foo'), 'qux')
            self.assertEqual(secrets.get_variable('sec'), 'ure')

    def test_variables_missing(self) -> None:
        """Check that missing variables are ignored."""
        with self._setup_secrets({
                'internal.yml': {'foo': 'qux', 'sec': 'none'},
                'secrets.yml': {'sec': 'ure'},
        }) as directory, mock.patch.dict(os.environ, {
            'INTERNAL_VARS_FILE': f'{directory}/internal.yml',
            'LEGACY_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertEqual(secrets.get_variable('foo'), 'qux')
            self.assertEqual(secrets.get_variable('sec'), 'ure')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_variables_encrypted(self) -> None:
        """Check that encrypted variables cannot be read."""
        with self._setup_secrets({
                'public.yml': {'bar': secrets.encrypt('baz')},
                'internal.yml': {'foo': secrets.encrypt('qux')},
                'secrets.yml': {'sec': secrets.encrypt('ure')},
        }) as directory, mock.patch.dict(os.environ, {
            'PUBLIC_VARS_FILE': f'{directory}/public.yml',
            'INTERNAL_VARS_FILE': f'{directory}/internal.yml',
            'LEGACY_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.get_variable('bar'))
            self.assertRaises(Exception, lambda: secrets.get_variable('foo'))
            self.assertRaises(Exception, lambda: secrets.get_variable('sec'))

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets(self) -> None:
        """Check that basic secret processing works."""
        with self._setup_secrets({
                'public.yml': {'bar': secrets.encrypt('baz')},
                'internal.yml': {'foo': secrets.encrypt('qux')},
                'secrets.yml': {'sec': secrets.encrypt('ure')},
        }) as directory, mock.patch.dict(os.environ, {
            'PUBLIC_VARS_FILE': f'{directory}/public.yml',
            'INTERNAL_VARS_FILE': f'{directory}/internal.yml',
            'SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.get_secret('bar'))
            self.assertRaises(Exception, lambda: secrets.get_secret('foo'))
            self.assertEqual(secrets.get_secret('sec'), 'ure')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_unencrypted(self) -> None:
        """Check that uncrypted secret processing fails."""
        with self._setup_secrets({
                'public.yml': {'bar': 'baz'},
                'internal.yml': {'foo': 'qux'},
                'secrets.yml': {'sec': 'ure'},
        }) as directory, mock.patch.dict(os.environ, {
            'PUBLIC_VARS_FILE': f'{directory}/public.yml',
            'INTERNAL_VARS_FILE': f'{directory}/internal.yml',
            'SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.get_secret('bar'))
            self.assertRaises(Exception, lambda: secrets.get_secret('foo'))
            self.assertRaises(Exception, lambda: secrets.get_secret('sec'))

    def test_secrets_invalid(self) -> None:
        """Check that invalid secret files fail."""
        with self._setup_secrets({
                'public.yml': ['bar', 'baz'],
        }) as directory, mock.patch.dict(os.environ, {
            'PUBLIC_VARS_FILE': f'{directory}/public.yml',
        }):
            with self.assertRaises(Exception, msg='invalid'):
                secrets.get_variable('bar')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_custom(self) -> None:
        """Check that custom secrets files work."""
        with self._setup_secrets({
                'custom-secrets.yml': {'bar': secrets.encrypt('baz')},
                'custom-secrets-2.yml': {'foo': secrets.encrypt('sec')},
                'custom-vars.yml': {'ure': 'qux'},
        }) as directory, mock.patch.dict(os.environ, {
            'CUSTOM_SECRETS_FILE': f'{directory}/custom-secrets.yml',
            'CUSTOM_SECRETS_FILE_2': f'{directory}/custom-secrets-2.yml',
            'CUSTOM_VARS_FILE': f'{directory}/custom-vars.yml',
            'CKI_SECRETS_NAMES': 'CUSTOM_SECRETS_FILE CUSTOM_SECRETS_FILE_2',
            'CKI_VARS_NAMES': 'CUSTOM_VARS_FILE CUSTOM_VARS_FILE_2',
        }):
            self.assertEqual(secrets.get_secret('bar'), 'baz')
            self.assertEqual(secrets.get_secret('foo'), 'sec')
            self.assertEqual(secrets.get_variable('ure'), 'qux')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_encrypt(self) -> None:
        """Check that basic encryption works."""
        self.assertEqual(secrets.encrypt('bar', salt=False), '0eA1mjLawgSmRg9bpsdPDw==')
        self.assertTrue(secrets.encrypt('bar').startswith('U2FsdGVkX1'))
