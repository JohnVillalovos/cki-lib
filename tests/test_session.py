"""get_session tests."""
import contextlib
import os
import tempfile
import unittest
from unittest import mock

import responses
from urllib3.util.retry import Retry

from cki_lib.logger import get_logger
from cki_lib.session import get_session


class TestSession(unittest.TestCase):
    """Test get_session."""

    @staticmethod
    @contextlib.contextmanager
    def _bundles(requests='', curl='', fedora='', debian=''):
        with mock.patch.multiple('cki_lib.session',
                                 FEDORA_BUNDLE=fedora, DEBIAN_BUNDLE=debian), \
                mock.patch.dict(os.environ, {
                    'REQUESTS_CA_BUNDLE': requests, 'CURL_CA_BUNDLE': curl}), \
                tempfile.NamedTemporaryFile() as file:
            yield file.name

    def test_bundle_none(self):
        """Test the verify field with no bundle overrides."""
        with self._bundles():
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_fedora(self):
        """Test the verify field with an existing Fedora bundle."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_fedora_missing(self):
        """Test the verify field with a missing Fedora bundle."""
        with self._bundles(), \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', 'missing'):
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_debian(self):
        """Test the verify field with an existing Debian bundle."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.DEBIAN_BUNDLE', name):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_debian_missing(self):
        """Test the verify field with a missing Debian bundle."""
        with self._bundles(), \
                mock.patch('cki_lib.session.DEBIAN_BUNDLE', 'missing'):
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_requests(self):
        """Test the verify field with an existing REQUESTS_CA_BUNDLE."""
        with self._bundles() as name, \
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': name}):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_requests_missing(self):
        """Test the verify field with a missing REQUESTS_CA_BUNDLE."""
        with self._bundles(), \
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')

    def test_bundle_requests_preferred(self):
        """Test that REQUESTS_CA_BUNDLE is preferred over system bundles."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name),\
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')

    def test_bundle_curl_preferred(self):
        """Test that CURL_CA_BUNDLE is preferred over system bundles."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name),\
                mock.patch.dict(os.environ, {'CURL_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')

    @responses.activate
    def test_logger_default(self):
        """Test that the logger defaults to the user agent."""
        responses.add('GET', 'http://dummy')
        session = get_session('agent')
        with self.assertLogs('cki.agent', 'DEBUG'):
            session.get('http://dummy')

    @responses.activate
    def test_logger_override(self):
        """Test that the logger can be overridden."""
        responses.add('GET', 'http://dummy')
        session = get_session('agent', logger=get_logger('logger'))
        with self.assertLogs('cki.logger', 'DEBUG'):
            session.get('http://dummy')

    @mock.patch('cki_lib.session.Retry', wraps=Retry)
    def test_get_session_retry(self, mock_retry):
        # Defaults work as expected.
        default_args = {'total': 5, 'backoff_factor': 1}
        get_session('agent')
        mock_retry.assert_called_with(**default_args)

        # Custom args work as expected.
        custom_args = {'total': 3, 'allowed_methods': False,
                       'status_forcelist': 504}
        get_session('agent', retry_args=custom_args)
        mock_retry.assert_called_with(**{**default_args, **custom_args})

    def test_get_session_raising(self):
        # No raising
        session = get_session('agent', raise_for_status=False)
        response_functions = [func.__name__ for func in session.hooks['response']]
        self.assertTrue('do_raising' not in response_functions)

        # Raise me
        session = get_session('agent', raise_for_status=True)
        response_functions = [func.__name__ for func in session.hooks['response']]
        self.assertTrue('do_raising' in response_functions)

    @responses.activate
    def test_get_session_timeout_default(self):
        """Check default session timeout."""
        responses.add('GET', 'http://dummy')
        response = get_session('agent').get('http://dummy')
        self.assertTrue(response.request.req_kwargs.get('timeout'), 300)

    @responses.activate
    def test_get_session_timeout_override(self):
        """Check overriding the session timeout."""
        responses.add('GET', 'http://dummy')
        response = get_session('agent').get('http://dummy', timeout=60)
        self.assertTrue(response.request.req_kwargs.get('timeout'), 60)

    @responses.activate
    @mock.patch('cki_lib.session.METRIC_REQUEST_ELAPSED')
    def test_metric_elapsed(self, mock_metric):
        """Test elapsed time is tracked in prometheus metric."""
        responses.add('GET', 'http://dummy.com/foo/bar')
        session = get_session('agent', logger=get_logger('logger'))
        session.get('http://dummy.com/foo/bar')
        mock_metric.assert_has_calls([
            mock.call.labels('dummy.com'),
            mock.call.labels().observe(mock.ANY)
        ])

        elapsed = mock_metric.mock_calls[1].args[0]
        self.assertTrue(isinstance(elapsed, float))
        self.assertAlmostEqual(elapsed, 0, places=2)
