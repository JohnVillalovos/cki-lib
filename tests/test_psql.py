"""Test cki_lib.psql module."""
import unittest
from unittest import mock

try:
    import psycopg2  # pylint: disable=import-error

    from cki_lib import psql
    NO_PSYCOPG2 = False
except ImportError:
    NO_PSYCOPG2 = True


@unittest.skipIf(NO_PSYCOPG2, 'psycopg2 is not installed')
class TestPSQLHandler(unittest.TestCase):
    """Test PSQLHandler."""

    def test_init(self):
        """Test init parameters."""
        cases = [
            (['psql.host', '1234', 'db'], {},
             'host=psql.host port=1234 dbname=db'),
            (['psql.host', '1234', 'db'], {'user': 'usr', 'password': 'pwd', 'require_ssl': True},
             'host=psql.host port=1234 dbname=db user=usr password=pwd sslmode=require'),
        ]

        for args, kwargs, expected in cases:
            handler = psql.PSQLHandler(*args, **kwargs)
            self.assertEqual(expected, handler.connection_string)
            self.assertIsNone(handler._connection)  # pylint: disable=protected-access
            self.assertTrue(handler.autocommit)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection_already_exists(self, mock_connect):
        # pylint: disable=protected-access
        """Test connection property. _connection already set."""
        handler = psql.PSQLHandler('psql.host', '1234', 'db')
        handler._connection = mock.Mock()
        handler._connection.closed = 0
        handler.connection  # pylint: disable=pointless-statement
        self.assertFalse(mock_connect.called)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection_already_closed(self, mock_connect):
        """Test connection property. _connection exists but it's closed."""
        # pylint: disable=protected-access
        handler = psql.PSQLHandler('psql.host', '1234', 'db')
        handler._connection = mock.Mock()
        handler._connection.closed = 2
        handler.connection  # pylint: disable=pointless-statement
        self.assertTrue(mock_connect.called)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection(self, mock_connect):
        """Test connection property."""
        connection = mock.Mock()
        mock_connect.return_value = connection

        handler = psql.PSQLHandler('psql.host', '1234', 'db')
        handler.connection  # pylint: disable=pointless-statement
        self.assertTrue(mock_connect.called)
        connection.set_isolation_level.assert_called_with(0)

    @mock.patch('cki_lib.psql.psycopg2.connect')
    def test_connection_no_autocommit(self, mock_connect):
        """Test connection property."""
        connection = mock.Mock()
        mock_connect.return_value = connection

        handler = psql.PSQLHandler('psql.host', '1234', 'db', autocommit=False)
        handler.connection  # pylint: disable=pointless-statement

        self.assertTrue(mock_connect.called)
        self.assertFalse(connection.set_isolation_level.called)

    @staticmethod
    @mock.patch('cki_lib.psql.psycopg2.connect', mock.Mock())
    def test_execute():
        """Test execute method."""
        handler = psql.PSQLHandler('host', 1234, 'db')
        handler._connection = mock.MagicMock()
        handler._connection.closed = 0

        handler.execute('SELECT %s', 'foo')
        handler.connection.assert_has_calls([
            mock.call.cursor(),
            mock.call.cursor().__enter__(),
            mock.call.cursor().__enter__().execute('SELECT %s', 'foo'),
            mock.call.cursor().__enter__().fetchall(),
            mock.call.cursor().__exit__(None, None, None)
        ])

    @mock.patch('cki_lib.psql.psycopg2.connect', mock.Mock())
    def test_execute_retry(self):
        """Test execute method retries the queries after InterfaceError."""
        handler = psql.PSQLHandler('host', 1234, 'db')
        handler._execute = mock.Mock()
        handler._execute.side_effect = psycopg2.InterfaceError

        self.assertRaises(psycopg2.InterfaceError, handler.execute, 'SELECT %s', 'foo')
        self.assertEqual(2, handler._execute.call_count)
