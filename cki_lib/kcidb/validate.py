"""CKI KCIDB Schema."""
import argparse
import json
import pathlib
import pkgutil

import jsonschema
import kcidb_io
import yaml

SCHEMA = yaml.safe_load(pkgutil.get_data(__name__, 'schema.yaml'))['schema']


def validate(data):
    """Validate data against CKI and KCIDB formats."""
    kcidb_io.schema.V4.validate(data)
    jsonschema.validate(
        instance=data,
        schema=SCHEMA,
        format_checker=jsonschema.draft7_format_checker
    )


def main(argv=None):
    """Run the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    args = parser.parse_args(argv)

    validate(json.loads(pathlib.Path(args.file).read_text(encoding='utf8')))


if __name__ == "__main__":
    main()
