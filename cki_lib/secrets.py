"""Implement secrets and variable access as used in the infrastructure repo."""
import argparse
import functools
import os
import pathlib
import subprocess
import typing

import yaml


@functools.lru_cache()
def _read_secrets_file(file_path: str) -> typing.Dict[str, typing.Any]:
    secrets = yaml.safe_load(pathlib.Path(file_path).read_text(encoding='utf8'))
    if not isinstance(secrets, dict):
        raise Exception('Invalid secrets file')
    return secrets


def _read_secrets_files(env_names: typing.Sequence[str]) -> typing.Dict[str, typing.Any]:
    result = {}
    for env_name in env_names:
        if env_name not in os.environ:
            continue
        result.update(_read_secrets_file(os.environ[env_name]))
    return result


def encrypt(value: str, *, salt: bool = True) -> str:
    """Encrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-e', '-a'
    ], encoding='utf8', input=value, stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def decrypt(value: str, *, salt: bool = True) -> str:
    """Decrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-d', '-a'
    ], encoding='utf8', input=value + '\n', stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def get_secret(key: str) -> typing.Any:
    """Return a decrypted secret from the secrets file."""
    return decrypt(_read_secrets_files(
        os.environ.get('CKI_SECRETS_NAMES', '').split() +
        ['SECRETS_FILE', 'LEGACY_SECRETS_FILE'])[key])


def get_variable(key: str) -> typing.Any:
    """Return an unencrypted variable from the variables files.

    Also supports unencrypted variables in the legacy secrets file (deprecated).
    """
    value = _read_secrets_files(
        os.environ.get('CKI_VARS_NAMES', '').split() +
        ['PUBLIC_VARS_FILE', 'INTERNAL_VARS_FILE', 'LEGACY_SECRETS_FILE'])[key]
    # check can be removed once legacy secrets file support is removed
    if isinstance(value, str) and value.startswith('U2FsdGVkX1'):
        raise Exception(f'Unable to access secret {key} via get_variable')
    return value.strip() if isinstance(value, str) else value


def _encrypt_cli(args: typing.Optional[typing.Sequence[str]] = None) -> None:
    """Encrypt a secret via the CLI."""
    parser = argparse.ArgumentParser(description='Encrypt a secret.')
    parser.add_argument('value', help='secret value')
    parsed_args = parser.parse_args(args)
    print(encrypt(parsed_args.value))


def _get_secret_cli(args: typing.Optional[typing.Sequence[str]] = None) -> None:
    """Return a decrypted secret from the secrets file via the CLI."""
    parser = argparse.ArgumentParser(description='Return a decrypted secret.')
    parser.add_argument('key', help='secret name')
    parsed_args = parser.parse_args(args)
    print(get_secret(parsed_args.key))


def _get_variable_cli(args: typing.Optional[typing.Sequence[str]] = None) -> None:
    """Return an unencrypted variable from the variables files via the CLI."""
    parser = argparse.ArgumentParser(description='Return an unencrypted variable.')
    parser.add_argument('key', help='variable name')
    parsed_args = parser.parse_args(args)
    print(get_variable(parsed_args.key))
