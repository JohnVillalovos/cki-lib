"""Pipeline trigger variables definition."""
import functools
from importlib.resources import files
import os

import yaml


@functools.cache
def _trigger_variables() -> list[str]:
    return yaml.safe_load(files('cki_lib').joinpath('trigger_variables.yml').read_bytes())


def pipeline_vars_from_env() -> dict[str, str]:
    """Return all trigger variables as dict."""
    return {var: os.environ[var] for var in _trigger_variables() if var in os.environ}


def _print_var_names() -> None:
    """Print names of trigger variables."""
    for var in _trigger_variables():
        print(var)


if __name__ == '__main__':
    _print_var_names()
