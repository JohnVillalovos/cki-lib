"""Helper for creating requests Session."""
import logging
import os.path
import typing
from urllib import parse

import prometheus_client as prometheus
import requests
from requests import adapters
from urllib3.util.retry import Retry

from .logger import get_logger

FEDORA_BUNDLE = '/etc/pki/tls/certs/ca-bundle.crt'
DEBIAN_BUNDLE = '/etc/ssl/certs/ca-certificates.crt'

METRIC_REQUEST_ELAPSED = prometheus.Histogram(
    'cki_request_elapsed',
    'Time spent making a request',
    ['hostname']
)


def get_session(user_agent: str,
                logger: typing.Optional[logging.Logger] = None,
                retry_args: typing.Optional[typing.Dict[str, typing.Any]] = None,
                raise_for_status: bool = False,
                ) -> requests.Session:
    """Return pre-configured requests Session.

    The session contains configured user_agent, retries
    and optional logging.

    By default, the requests are logged with DEBUG priority, either by the
    explicitly specified logger or a logger instantiated with the same name as
    the user agent. This can be disabled by passing logger=None.
    """
    session = requests.Session()
    session.headers.update({'User-Agent': user_agent})

    logger_instance = logger or get_logger(user_agent)

    def log_request(response: requests.Response, *_: typing.Any, **__: typing.Any) -> None:
        request_duration = response.elapsed.total_seconds()
        logger_instance.debug(
            '[%.3fs] Requested: %s', request_duration, response.url
        )

        hostname = parse.urlsplit(response.url).netloc
        METRIC_REQUEST_ELAPSED.labels(hostname).observe(request_duration)

    session.hooks['response'].append(log_request)

    if raise_for_status:
        def do_raising(response: requests.Response, *_: typing.Any, **__: typing.Any) -> None:
            response.raise_for_status()
        session.hooks['response'].append(do_raising)

    default_retry_args: typing.Dict[str, typing.Any] = {'total': 5, 'backoff_factor': 1}
    default_retry_args.update(retry_args or {})

    retry = Retry(**default_retry_args)
    adapter = _TimeoutHTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    # By default, requests uses certificates from REQUESTS_CA_BUNDLE,
    # CURL_CA_BUNDLE, or the certifi package. Distributions divert the certifi
    # default to
    #   Fedora: /etc/pki/tls/certs/ca-bundle.crt
    #   Debian: /etc/ssl/certs/ca-certificates.crt
    # With a pip-installed requests, this is not the case. In that case, any
    # certs added to the system certificate bundle are ignored unless
    # REQUESTS_CA_BUNDLE is explicitly set. Do The Right Thing in this case and
    # use any of the distribution bundles if found.

    bundle = (os.environ.get('REQUESTS_CA_BUNDLE') or
              os.environ.get('CURL_CA_BUNDLE') or
              (os.path.isfile(FEDORA_BUNDLE) and FEDORA_BUNDLE) or
              (os.path.isfile(DEBIAN_BUNDLE) and DEBIAN_BUNDLE))
    if bundle:
        session.verify = bundle

    return session


class _TimeoutHTTPAdapter(adapters.HTTPAdapter):
    def send(
        self,
        request: requests.PreparedRequest,
        stream: bool = False,
        timeout: typing.Union[None, float, typing.Tuple[float, float],
                              typing.Tuple[float, None]] = None,
        verify: typing.Union[bool, str] = True,
        cert: typing.Union[None, bytes, str,
                           typing.Tuple[typing.Union[bytes, str], typing.Union[bytes, str]]] = None,
        proxies: typing.Optional[typing.Mapping[str, str]] = None,
    ) -> requests.Response:
        # pylint: disable=too-many-arguments
        return super().send(request, stream=stream, timeout=timeout or 300,
                            verify=verify, cert=cert, proxies=proxies)
