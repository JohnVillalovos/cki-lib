"""Misc utility functions."""
import argparse
import contextlib
from contextlib import contextmanager
from datetime import datetime
from datetime import timedelta
import faulthandler
import fcntl
import logging
import os
import re
import signal
import subprocess
import tempfile

from dateutil.tz import UTC

from .logger import get_logger
from .retrying import retrying_on_exception
from .session import get_session

LOGGER = get_logger(__name__)
FAST_SESSION = get_session(__name__,
                           retry_args={'backoff_factor': 0})

_TIMEDELTA_REGEX = re.compile(r'((?P<weeks>[\.\d]+?) *(?:w|weeks?))? *'
                              r'((?P<days>[\.\d]+?) *(?:d|days?))? *'
                              r'((?P<hours>[\.\d]+?) *(?:h|hours?))? *'
                              r'((?P<minutes>[\.\d]+?) *(?:m|mins?|minutes?))? *'
                              r'((?P<seconds>[\.\d]+?) *(?:s?|secs?|seconds?))?')


class EnvVarNotSetError(Exception):
    """Requested environment variable is not set."""


@contextlib.contextmanager
def only_log_exceptions(exceptions=Exception):
    """Log, but ignore exceptions."""
    try:
        yield
    except exceptions:  # pylint: disable=broad-except
        LOGGER.exception('Ignored by context manager')


def shorten_url(url):
    """Attempt to shorten a URL."""
    with only_log_exceptions():
        shortener_url = os.environ.get('URL_SHORTENER_URL')
        if shortener_url:
            shortener_token = os.environ.get('URL_SHORTENER_TOKEN')
            headers = {'Authorization': f'Bearer {shortener_token}'}
            response = FAST_SESSION.post(shortener_url,
                                         headers=headers,
                                         json={'url': url},
                                         timeout=1.0)
            response.raise_for_status()
            url = response.text.strip()
    return url


def get_nested_key(data, key, default=None, *, lookup_attrs=False, delimiter='/'):
    """
    Dig through nested dictionaries/lists to get the value of a key.

    Inputs a key slash-separated like key_a/key_b/key_c, and returns 'value' on
    the following chain: {'key_a': {'key_b': {'key_c': value}}} handling
    missing keys.

    The default delimiter is / to play well with dot-prefix "internal" keys used
    in quite some places.

    Setting lookup_attrs=True will also retrieve object attributes.
    """
    subkeys = key.split(delimiter)
    for subkey in subkeys:
        try:
            data = data[int(subkey)] if isinstance(data, list) else data[subkey]
        except (KeyError, TypeError, IndexError, ValueError):
            if not lookup_attrs:
                return default
            try:
                data = getattr(data, subkey)
            except AttributeError:
                return default

    return data


def set_nested_key(data, key, value):
    """
    Set key on dictionary.

    Handle setting subkeys using / as delimiter. If sub dictionary does not
    exists, it will be created.
    """
    subkeys = key.split('/')
    for i, subkey in enumerate(subkeys):
        # On the last subkey, set the value.
        if i == len(subkeys) - 1:
            data[subkey] = value
            break

        # If not the last one, try to get or create subkey value.
        if subkey not in data:
            data[subkey] = {}

        if not isinstance(data[subkey], dict):
            raise Exception('Element is not a dictionary')

        data = data[subkey]


def append_to_nested_list(data, key, element):
    """
    Append an element to a list in the dictionary.

    Handle setting subkeys using / as delimiter. If sub dictionary does not
    exists, it will be created.

    If the final list does not exist, it will be created.
    """
    value = get_nested_key(data, key, [])
    if not isinstance(value, list):
        raise Exception('Element is not a list')
    value += [element]
    set_nested_key(data, key, value)


def get_env_var_or_raise(env_key):
    """Retrieve the value of an environment variable or raise exception.

    Args:
        env_key:      Name of the variable's value to retrieve.

    Returns:
        Value of the environment variable, if set.

    Raises:
        EnvVarNotSetError if the variable is not set.

    """
    if not (env_var := os.getenv(env_key)):
        raise EnvVarNotSetError(f'Environment variable {env_key} is not set!')
    return env_var


def booltostr(value):
    """Convert a boolean to True/False strings."""
    return str(bool(value)).lower()


def strtobool(value: str) -> bool:
    """Convert True/False strings to bool."""
    if value in {'true', 'True'}:
        return True
    if value in {'false', 'False'}:
        return False
    raise ValueError(f'invalid truth value {value}')


def parse_timedelta(value: str) -> timedelta:
    """Convert time deltas like "3m 5s" to datetime.timedelta."""
    if not (parts := _TIMEDELTA_REGEX.fullmatch(value)):
        raise Exception(f"Unable to parse time delta '{value}'")
    time_params = {name: float(param) for name, param in parts.groupdict().items() if param}
    if not time_params:
        raise Exception("Unable to parse empty time delta")
    return timedelta(**time_params)


def get_env_bool(name: str, default: bool = False) -> bool:
    """Convert the value of an environment variable to bool."""
    return strtobool(os.environ.get(name, str(default)))


def get_env_int(name, default):
    """Convert the value of an environment variable to int."""
    return int(os.environ.get(name, str(default)))


def is_production() -> bool:
    """Check whether CKI_DEPLOYMENT_ENVIRONMENT == production."""
    return deployment_environment() == 'production'


def deployment_environment() -> str:
    """Return the deployment environment, defaults to development."""
    if environment := os.getenv('CKI_DEPLOYMENT_ENVIRONMENT'):
        return environment
    # backwards compatibility with IS_PRODUCTION
    return 'production' if get_env_bool('IS_PRODUCTION', False) else 'development'


@contextmanager
def tempfile_from_string(data):
    """Create a tempfile that is deleted on contextmanager exit.

    Arguments:
        data: str, a string that the tempfile will contain

    """
    with tempfile.NamedTemporaryFile(delete=False) as temp:
        temp.write(data)
        temp.close()
        try:
            yield temp.name
        finally:
            os.unlink(temp.name)


@contextmanager
def enter_dir(directory):
    """Change directory using os.chdir(directory), return on context exit.

    Arguments:
        directory: str, a directory to enter

    """
    current = os.getcwd()
    try:
        os.chdir(directory)
        yield
    finally:
        os.chdir(current)


def safe_popen(*args, stdin_data=None, **kwargs):
    """Open a process with specified arguments, keyword arguments, stdin data.

    This function blocks until process finishes. Uses utf-8 dst_file decode
    stdout/stderr, if there's any output on them.

    Arguments:
        args:       arguments dst_file pass dst_file Popen
        stdin_data: None or str, use None when you don't want dst_file pass
                    string data dst_file stdin
        kwargs:     keyword arguments dst_file pass dst_file Popen
    Returns:
        tuple (stdout, stderr, returncode) where
            stdout is a string
            stderr is a string
            returncode is an integer

    """
    with subprocess.Popen(*args, **kwargs) as subproc:
        stdout, stderr = subproc.communicate(stdin_data)
        stdout = stdout.decode('utf-8') if stdout else ''
        stderr = stderr.decode('utf-8') if stderr else ''
        return stdout, stderr, subproc.returncode


def read_stream(stream):
    """Read lines from stream like stdout."""
    fhandle = stream.fileno()
    flags = fcntl.fcntl(fhandle, fcntl.F_GETFL)
    fcntl.fcntl(fhandle, fcntl.F_SETFL, flags | os.O_NONBLOCK)
    try:
        data = stream.read()
        if data:
            return data.decode('utf-8')
    except OSError:
        pass
    return ""


@retrying_on_exception(RuntimeError)
def retry_safe_popen(err_exc_strings, *args, stdin_data=None, **kwargs):
    """Call safe_popen with *args, stdin_data=None, **kwargs provided.

    If stderr stream is present and contains any string in err_exc_strings
    list, then the process call is done again with retry after 3 seconds
    (see retrying_on_exception decorator). Log commands retry and allow 3
    retries max. Also log if last command failed and we gave up. The
    program execution is not terminated / no exception is raised on last
    failure.

    Args:
        err_exc_strings: a list of strings; if any is present in stderr,
                         retry the command
        args:            arguments to pass to Popen
        stdin_data:      None or str, use None when you don't want to pass
                         string data to stdin
        kwargs:          keyword arguments to pass to Popen
    Returns:
        tuple (stdout, stderr, returncode) where
            stdout is a string
            stderr is a string
            returncode is an integer
    """
    stdout, stderr, returncode = safe_popen(*args, stdin_data=stdin_data,
                                            **kwargs)

    if err_exc_strings and stderr:
        # we clearly want to catch issues; let's debug what stderr was
        logging.warning(stderr.strip())

    for err_str in err_exc_strings:
        if stderr and err_str in stderr:
            logging.warning('caught "%s" error string in stderr', err_str)
            raise RuntimeError

    return stdout, stderr, returncode


class StoreNameValuePair(argparse.Action):
    # pylint: disable=too-few-public-methods
    """Parse key=value arguments."""

    def __call__(self, parser, namespace, values, option_string=None):
        """Parse the command line argument."""
        variables = getattr(namespace, self.dest) or {}
        if isinstance(values, str):
            values = [values]
        for key_value_pair in values:
            key, value = key_value_pair.split('=', 1)
            variables[key] = value
        setattr(namespace, self.dest, variables)


def sentry_init(sentry_sdk, **kwargs):
    """
    Initialize sentry and set the correct environment.

    Optionally, kwarg parameters are added to the sentry_sdk.init call.

    Additionally the faulthandler module is connected to SIGUSR1
    to allow to obtain backtraces from running processes.
    """
    params = {
        'ca_certs': os.getenv('REQUESTS_CA_BUNDLE'),
        'environment': deployment_environment(),
    }
    params.update(kwargs)
    if ci_job_url := os.getenv('CI_JOB_URL'):
        params['server_name'] = ci_job_url

    sentry_sdk.init(**params)

    faulthandler.register(signal.SIGUSR1)


def sentry_flush():
    """
    Send all events pending in sentry.

    If sentry_sdk is available and it's initialized, flush it.

    https://docs.sentry.io/platforms/python/configuration/draining/
    """
    try:
        from sentry_sdk import Hub  # pylint: disable=import-outside-toplevel
    except ImportError:
        return

    client = Hub.current.client
    if client is not None:
        client.flush()


def utc_now_iso():
    """Return UTC time in ISO 8601 format used by KCIDB."""
    return datetime.now(UTC).isoformat()


def key_value_list_to_dict(kv_list):
    """Turn a list of {key: key, value: value} elements into a dictionary."""
    return {v['key']: v['value'] for v in kv_list or []}


def _is_production_cli() -> int:
    """Exit with 0 if CKI_DEPLOYMENT_ENVIRONMENT == production, else with 1."""
    return 0 if is_production() else 1


def _deployment_environment_cli() -> None:
    """Print the deployment environment."""
    print(deployment_environment())
