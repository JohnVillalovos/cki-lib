"""Expose multi-process metrics collected from env[PROMETHEUS_MULTIPROC_DIR]."""
import os
import pathlib
import typing

import prometheus_client
import prometheus_client.multiprocess


def wipe_dbfiles() -> None:
    """Remove stale metric files.

    The multiprocess readme wants the metrics directory to be wiped between
    process/Gunicorn runs.
    """
    path = pathlib.Path(os.environ.get('PROMETHEUS_MULTIPROC_DIR') or
                        os.environ['prometheus_multiproc_dir'])
    if not path.is_dir():
        return
    for dbfile in pathlib.Path(path).iterdir():
        dbfile.unlink()


def run(
    _,
    start_response: typing.Callable[[str, typing.List[typing.Tuple[str, str]]], None],
) -> None:
    """Run main handler."""
    wipe_dbfiles()
    registry = prometheus_client.CollectorRegistry()
    prometheus_client.multiprocess.MultiProcessCollector(registry)
    data = prometheus_client.generate_latest(registry)
    status = '200 OK'
    response_headers = [
        ('Content-type', prometheus_client.CONTENT_TYPE_LATEST),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    return iter([data])
