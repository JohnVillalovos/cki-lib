#!/bin/bash
set -euo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")"

# shellcheck source-path=SCRIPTDIR
. ../cki_utils.sh
# shellcheck source-path=SCRIPTDIR
. helpers.sh

cki_say "functions"

_failed_init

function check_cki_git_clean_url() {
    _check_equal "$(cki_git_clean_url "$1")" "$2" "cki_git_clean_url" "Is cki_git_clean_url $3"
}
check_cki_git_clean_url "https://foo.bar/test.git/" "https://foo.bar/test.git/" "passing through correct URLs"
check_cki_git_clean_url "https://foo.bar/test.git"  "https://foo.bar/test.git/" "appending a slash"
check_cki_git_clean_url "https://foo.bar/test"      "https://foo.bar/test.git/" "appending .git/"
check_cki_git_clean_url "https://foo.bar/test/"     "https://foo.bar/test.git/" "appending .git with an existing slash"
check_cki_git_clean_url "foo.bar/test.git/"         "https://foo.bar/test.git/" "prepending the protocol"

function check_cki_is_true() {
    declare -A IS_TRUE_MAPPING
    IS_TRUE_MAPPING["true"]=0
    IS_TRUE_MAPPING["True"]=0
    IS_TRUE_MAPPING["false"]=1
    IS_TRUE_MAPPING["False"]=1
    IS_TRUE_MAPPING["randomvalue"]=1

    for VALUE in "${!IS_TRUE_MAPPING[@]}"; do
        cki_is_true "${VALUE}" && RETURNED=0 || RETURNED=$?
        _check_equal "${RETURNED}" "${IS_TRUE_MAPPING[${VALUE}]}" "cki_is_true returned" "Is the cki_is_true value correct for ${VALUE}"
    done
}
check_cki_is_true

# function() instead of function{} so that the stub functions are scoped
function check_cki_update_repo()
(
    function git {
        /usr/bin/git "$@" > /dev/null 2>&1
        local git_params=("$@")
        declare -p git_params >&2
    }
    TEST_OUTPUT=$(mktemp -d)
    trap 'rm -rf "${TEST_OUTPUT}"' EXIT

    eval "$(cd "${TEST_OUTPUT}" && cki_update_repo https://gitlab.com/cki-project/inventory.git 2>&1 >/dev/null)"
    _check_equal "$(stat "${TEST_OUTPUT}/inventory/.git/config" > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"

    git_params=()
    eval "$(cd "${TEST_OUTPUT}" && cki_update_repo https://gitlab.com/cki-project/inventory.git 2>&1 >/dev/null)"
    _check_equal "${git_params[0]}" "pull" "param" "Does cki_repo_update call git pull if the repo exists"

    git_params=()
    eval "$(cd "${TEST_OUTPUT}" && cki_update_repo https://gitlab.com/cki-project/inventory.git 2>&1 >/dev/null)"
    _check_equal "${#git_params[@]}" "0" "invocations" "Does cki_repo_update skip the git call if the repo is up to date"

    rm -rf "${TEST_OUTPUT}/inventory"
    export inventory_pip_url="https://gitlab.com/mh21/inventory@main"
    eval "$(cd "${TEST_OUTPUT}" && cki_update_repo https://gitlab.com/cki-project/inventory.git 2>&1 >/dev/null)"
    _check_equal "$(stat "${TEST_OUTPUT}/inventory/.git/config" > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"
    _check_equal "$(GIT_DIR="${TEST_OUTPUT}/inventory/.git" /usr/bin/git config remote.origin.url)" "https://gitlab.com/mh21/inventory.git/" "remote url" "Does cki_repo_update use the overridden repo"
    _check_equal "$(GIT_DIR="${TEST_OUTPUT}/inventory/.git" /usr/bin/git symbolic-ref --short HEAD)" "override" "branch" "Does cki_repo_update use the overridden branch"
)
check_cki_update_repo

_failed_check

